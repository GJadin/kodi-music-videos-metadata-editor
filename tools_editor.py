# -*- coding: utf-8 -*-

#  Copyright (C) 2021 Guillaume Jadin
import sys
from os import listdir
from os.path import isdir, isfile, join

class Colors:
    """
    Collection of colors to colorize logs
    """
    RESET = '\033[0m'
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    BLUE = '\033[94m'
    PURPLE = '\033[95m'
    CYAN = '\033[96m'


class Logs:
    """
    Display Logs in a structured way
    """

    @staticmethod
    def paths(need_mv=True, need_m=True):
        """
        Display all the paths used

        :param bool need_mv: Show the Music Video Path or not
        :param bool need_m: Show the Music Path or not
        :rtype int
        :return the separator size
        """
        print_mv = "    Music Videos : " + f"{Colors.PURPLE}" + Paths.MUSIC_VIDEOS + f"{Colors.RESET}" if need_mv else ""
        print_m = "    Musics       : " + f"{Colors.PURPLE}" + Paths.MUSICS + f"{Colors.RESET}" if need_m else ""
        longest_str = (len(print_m) if len(print_m) > len(print_mv) else len(print_mv)) - 5
        print("-" * longest_str, file=sys.stderr)
        if need_mv: print(print_mv, file=sys.stderr)
        if need_m: print(print_m, file=sys.stderr)
        print("-" * longest_str, file=sys.stderr)
        return longest_str

    @staticmethod
    def abort():
        """
        Display an abort message
        """
        print(f"{Colors.RED}Abort.{Colors.RESET}", file=sys.stderr)

    @staticmethod
    def resume(types_file):
        """
        Display the resume of the program

        :type types_file: [(str, int, str)]
        :param types_file: a list of type of files with:
            types_file[x][0] => label name
            types_file[x][1] => nbr of file treated
            types_file[x][2] => label color (in the Colors class).
        """
        data = f"{Colors.RESET}" + " [{0} file(s): "
        tot_files = 0
        for i, type_file in enumerate(types_file):
            data += f"{type_file[2]}" + "{0} {1}".format(type_file[1], type_file[0]) + f"{Colors.RESET}"
            tot_files += type_file[1]
            if i < len(types_file) - 1: data += ", "
        data += "]"
        print(f"{Colors.GREEN}Process Complete{Colors.RESET}" + data.format(tot_files), file=sys.stderr)

    @staticmethod
    def status(options):
        """
        Display a status

        :param (str, str, str) options: a tuple of options
                options[0] => label name
                options[1] => label msg
                options[2] => label color (in the Colors class).
        """
        print(f"{options[2]}" + "[{0}]  {1}".format(options[0], options[1]) + f"{Colors.RESET}", file=sys.stderr)

    @staticmethod
    def status_file(filename, options):
        """
        Display the status of one file

        :param str filename: a filename
        :param (str, str, str) options: a tuple of options
                options[0] => label name
                options[1] => label msg
                options[2] => label color (in the Colors class).
        """
        print(f"{options[2]}" + "[{0}]  {1} => {2}".format(options[0], options[1], filename) + f"{Colors.RESET}",
              file=sys.stderr)

    @staticmethod
    def pcolor(text, color):
        """
        Display the text in a specific color

        :param str text: a string
        :param str color: a color in the Colors class
        """
        print(f"{color}" + text + f"{Colors.RESET}", file=sys.stderr)

    @staticmethod
    def icolor(text, color):
        """
        Display the text in a specific color and return the user answer

        :param str text: a string
        :param str color: a color in the Colors class
        :return: the user answer
        :rtype: str
        """
        return input(f"{color}" + text + f"{Colors.RESET}")

class FileParser:
    """
    Functions which help to manage filename
    """

    @staticmethod
    def filter_files(curr_path, files, wanted_ext_file):
        """
        Keep files in the list "files" which have an extension equal at "wanted_ext_file"

        :param str curr_path: the path where the files are (NOT USED)
        :param list[str] files: a filenames list
        :param str wanted_ext_file: a string with the format ".EXTENSION" (EX: '.mp3', '.mp4', '.nfo', ...)
        :return: a list with all the filenames with the extension "wanted_ext_file".
                 if there is no elements with the extension "wanted_ext_file", return an empty list [].
        :rtype: list[str]
        """
        correct_ext = []
        for file in files:
            filename, curr_ext = FileParser.sep_extension(file)
            if curr_ext == wanted_ext_file:
                correct_ext.append(file)
        return correct_ext

    @staticmethod
    def sep_extension(file):
        """
        Separate the file extension with the filename, with the last dot of the file.

        :param str file: a file (a filename with an extension)
        :return: (filename, extension with the dot)
        :rtype: (str, str)
        """
        last_dot_index = file.rfind(".")
        return file[:last_dot_index], file[last_dot_index:]

    @staticmethod
    def sep_artist_title(filename):
        """
        Separate the artist with the song title, with the " - " format between them.

        :param str filename: a filename (without it extension)
        :return: (artist, song title)
        :rtype: (str, str)
        """
        return tuple(filename.split(" - "))

class FilenameEditor:
    """
    Functions which help to edit music videos files.
    """
    @staticmethod
    def get_music_videos_labels(): return ["official", "officiel", "video", "clip", "hd", "remastered", "4k"]
    @staticmethod
    def get_musics_labels(): return ["radio", "album", "live", "extended", "extend"]
    @staticmethod
    def get_feat_labels(): return ["feat", "ft."]

    @staticmethod
    def has_strange_char(file):
        """
        Check if the file has strange chars
        Chars treated : "—" "•" "|"

        :param str file: a file or a filename
        :return: True if file has strange chars, False otherwise
        :rtype: bool
        """
        for strange in ["—", "•", "|"]:
            if file.find(strange) != -1: return True
        return False

    @staticmethod
    def replace_strange_char(status_code, file):
        """
        Replace strange chars by their more conventional and similar chars (IF THEY EXIST !!)
        Chars replacement : "—" => "-"

        :param int status_code: edited (0), not edited (1)
        :param str file: a file or a filename
        :return: the (un)changed status code and the (un)edited file
        :rtype: (int, str)
        """
        sc, edit_file = status_code, file
        i = file.find(" — ")
        if i != -1: sc, edit_file = 0, file[:i] + " - " + file[i + 3:]
        return sc, edit_file

    @staticmethod
    def delete_forbidden_char(filename):
        """
        Delete chars which can cause a crash when calling system functions
        Chars treated :
            -> '.' at the end of a filename
            -> ' ?' and '?'
            -> '\', '/', '"', ':', '|', '>', '<', '*'

        :param str | [str] filename: a filename (without extension) or a list of filename
        :return: the (un)edited file
        :rtype: str | list(str)
        """
        names = filename if type(filename) == list else [filename]
        for i in range(0, len(names)):
            for c in [' ?', '?', '\\', '/', '\"', ':', '|', '>', '<', '*']:
                names[i] = names[i].replace(c, "")
            while names[i][-1] == '.':
                names[i] = names[i][:-1]
        return names if type(filename) == list else names[0]

    @staticmethod
    def delete_labels(status_code, filename, labels):
        """
        Delete music videos labels found often at the end of a Youtube Video (defined form var "labels")
        Examples: "(Official Music Video)", "[OFFICIAL]", "[HD VIDEO]", "[REMASTERED]", ...
        The labels are always between () or [] !!!

        :param int status_code: edited (0), not edited (1)
        :param str filename: a filename (WITHOUT extension)
        :param list(str) labels: a list of string which are recognition words to find in the string 'file'
        :return: the (un)changed status code and the (un)edited file
        :rtype: (int, str)
        """

        def is_not_limit(c, labels_temp):
            iter_label = 0
            for elem in labels_temp:
                if c != elem: iter_label += 1
            return False if iter_label == len(labels_temp) - 1 else True

        def is_not_start_limit(c):
            return is_not_limit(c, ["(", "["])

        def is_not_end_limit(c):
            return is_not_limit(c, [")", "]"])

        low_file = filename.lower()
        new_status_code, new_filename = status_code, filename
        for label in labels:
            i_label = low_file.rfind(label)
            if i_label != -1:
                lo, hi = i_label, i_label
                while is_not_start_limit(low_file[lo]) or is_not_end_limit(low_file[hi]):
                    if is_not_start_limit(low_file[lo]):
                        if lo == 0: # FILENAME LIMIT (START)
                            return new_status_code, new_filename
                        lo -= 1
                    if is_not_end_limit(low_file[hi]):
                        if hi == len(low_file) - 1: # FILENAME LIMIT (END)
                            return new_status_code, new_filename
                        hi += 1

                if not (is_not_start_limit(low_file[lo])) and not (is_not_end_limit(low_file[hi])):
                    if hi < len(low_file) - 1 and low_file[hi + 1] != '.':
                        new_status_code, new_filename = 0, new_filename[:lo].rstrip(" ") + " " + new_filename[hi + 1:].strip(" ")
                    new_status_code, new_filename = 0, new_filename[:lo].rstrip(" ") + new_filename[hi + 1:].strip(" ")
        return new_status_code, new_filename

class FileMatcher:
    """
    Functions which help to match folders/files between the music videos directory and the musics directory.
    """

    @staticmethod
    def search_dir(path, wanted_dir):
        """
        Search if a directory exists in the current path (WITHOUT CASE SENSITIVE)

        :param str path: the path where the directory should be (where to search)
        :param str wanted_dir: the directory wanted
        :return: if found the name of the correct directory else None
        :rtype: str | None
        """
        wanted_dir_lo = wanted_dir.lower()
        for dirname in listdir(path):
            if isdir(join(path, dirname)):
                if wanted_dir_lo == dirname.lower():
                    return dirname
        return None

    @staticmethod
    def search_file(path, wanted_file, delete_labels=False):
        """
        Search if a file exists in the current path (WITHOUT CASE SENSITIVE)
        Executing:
        (1) Check Exact Name
        if delete_labels == True:
            (2) Remove Labels MUSICS
            (3) Check Parsed Name (Don't return)
            if not:
                (4) Remove Labels FEATS
                (5) Check Parsed Name (Don't return)

        :param str path: the path where the file should be (where to search)
        :param str wanted_file: the file wanted (WITH extension)
        :param bool delete_labels: Delete labels to increase chances to find the file
        :return: if found the name of the correct file: (is_feat_parsed, filename) else (is_feat_parsed, None)
        :rtype: (bool, str) | (bool, None)
        """
        wanted_file_lo = wanted_file.lower()
        maybe_is_parsed, maybe_wanted_file = False, None
        for file in listdir(path):
            if isfile(join(path, file)):
                if wanted_file_lo == file.lower():              # Check Exact Name
                    return False, file

                if delete_labels:                               # Check Without Labels
                    sub_file = FileParser.sep_extension(file)
                    nolabel_file = FilenameEditor.delete_labels(1, sub_file[0], FilenameEditor.get_musics_labels())[1]
                    nolabel_file += sub_file[1]

                    if wanted_file_lo == nolabel_file.lower():  # Check if correspond
                        maybe_is_parsed, maybe_wanted_file = False, file

                    else:                                       # If it not corresponds, Check Without Feat
                        nolabel_file = FilenameEditor.delete_labels(1, nolabel_file, FilenameEditor.get_feat_labels())[1]
                        if wanted_file_lo == nolabel_file.lower():
                            maybe_is_parsed, maybe_wanted_file = True, file
        return maybe_is_parsed, maybe_wanted_file

class Paths:
    """
    Collection with all paths needed
    :cvar MUSIC_VIDEOS: Path to the music videos library.
    :cvar MUSICS: Path to the musics library.
                  Directories/Files in this path must have one of following structures:
                      {PATH}/{FIRST_LETTER_ARTIST}/{ARTIST}/{ARTIST} - {TITLE}.mp3
                      {PATH}/{ARTIST}/{ARTIST} - {TITLE}.mp3
                  Examples : /home/user/Music/G/Genesis/Genesis - Jesus He Knows Me.mp3
                             /home/user/Music/Genesis/Genesis - Jesus He Knows Me.mp3
    """
    # TODO: Change these paths to match with your system
    MUSIC_VIDEOS = r"/home/guillaume/Videos/Music Videos/"
    MUSICS = r"/home/guillaume/Musics/"
