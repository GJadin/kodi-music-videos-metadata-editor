#!/usr/bin/python3
# -*- coding: utf-8 -*-

#  Copyright (C) 2021 Guillaume Jadin
import sys, textwrap, eyed3, argparse
import tools_editor as tools
from tools_editor import Colors, Paths
from os import walk, rename, mkdir
from os.path import join, exists
from shutil import move


def Arguments():
    parser = argparse.ArgumentParser(description="Program used to rename and store songs into folders",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-rn", "--rename", action="store_true",
                        help="""RENAME MODE: Rename all files with the specified template sets with '-t' or'--template'.
    (Standalone)""")

    parser.add_argument("-t", "--template", type=str, default="{a} - {t}", help=textwrap.dedent("""Define the template used to rename files (Only used for RENAME MODE):
    \t {a} -> Set the ARTIST,
    \t {t} -> Set the TITLE,
    \t {l} -> Set the ALBUM.
    Default  => '{a} - {t}'"""))

    parser.add_argument("-rst", "--restructure", action="store_true",
                        help="""RESTRUCTURE MODE: Store Songs into folders with this structure:
    ./[FIRST LETTER OF THE ARTIST]/
        [ARTIST NAME]/
            [MUSIC FILES]
    (Standalone)""")

    parser.add_argument("-mdb", "--musicDB", type=str, default=None, help="Define the music path.")
    return parser.parse_args()

def restructure_songs(path, file):
    curr_path_file = join(path, file)
    mp3file_tags = eyed3.load(curr_path_file).tag

    artist = tools.FilenameEditor.delete_forbidden_char(mp3file_tags.artist)

    curr_path = join(path, artist[0].upper())
    if not exists(curr_path):
        mkdir(curr_path)

    curr_path = join(curr_path, artist)
    if not exists(curr_path):
        mkdir(curr_path)

    song_file_searched = tools.FileMatcher.search_file(curr_path, file)
    if song_file_searched is not None:
        tools.Logs.status(("EXIST", " "+file, Colors.YELLOW))
        return 1
    else:
        move(curr_path_file, join(curr_path, file))
        tools.Logs.status(("STORED", file, Colors.GREEN))
        return 0


def rename_songs(curr_path, file):
    curr_path_music = join(curr_path, file)
    mp3file_tags = eyed3.load(curr_path_music).tag
    artist, title, album = tools.FilenameEditor.delete_forbidden_char(
        [mp3file_tags.artist, mp3file_tags.title, mp3file_tags.album])

    ext = tools.FileParser.sep_extension(file)[1]
    new_filename = args.template.format(a=artist, t=title, l=album) + ext
    new_path_music = join(curr_path, new_filename)

    if file == new_filename:
        tools.Logs.status(("UNCHANGED", new_filename, Colors.YELLOW))
        return 1
    else:
        rename(curr_path_music, new_path_music)
        tools.Logs.status_file(new_filename, ("RENAMED", "  "+file, Colors.GREEN))
        return 0

def run():
    eyed3.log.setLevel("ERROR")
    if not exists(Paths.MUSICS):
        raise FileExistsError("The MUSIC path doesn't exists: \"{0}\"".format(Paths.MUSICS))

    if args.rename:
        labels = ("renamed", "unchanged")
    else: #args.restructure
        labels = ("stored", "already exist")

    types_files = [0, 0]
    curr_path, dirs, files = sorted(walk(Paths.MUSICS, topdown=False))[0]
    for file in files:
        if args.rename:
            types_files[rename_songs(curr_path, file)] += 1
        elif args.restructure:
            types_files[restructure_songs(curr_path, file)] += 1

    tools.Logs.resume([(labels[0], types_files[0], Colors.GREEN),
                       (labels[1], types_files[1], Colors.YELLOW)])

if __name__ == "__main__":
    args = Arguments()
    if args.musicDB is not None:
        Paths.MUSICS = args.musicDB
    Paths.MUSICS.encode('unicode_escape')
    sep_size = 0

    if args.rename:
        sep_size = tools.Logs.paths(need_mv=False)
        print("    TEMPLATE   " + f"{Colors.BLUE}" + args.template + f"{Colors.RESET}", file=sys.stderr)
        print("-" * sep_size, file=sys.stderr)
        answer = input(f"Launch the {Colors.GREEN}RENAME{Colors.RESET} mode? [y/N] ")
    elif args.restructure:
        sep_size = tools.Logs.paths(need_mv=False)
        answer = input(f"Launch the {Colors.YELLOW}RESTRUCTURE{Colors.RESET} mode? [y/N] ")
    else:
        tools.Logs.pcolor("error: No command specified\nTry './songs_editor --help' for more information.", Colors.RED)
        exit(1)

    if answer.lower() == "y":
        run()
    else:
        tools.Logs.abort()
