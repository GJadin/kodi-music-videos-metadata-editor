#!/usr/bin/python3
# -*- coding: utf-8 -*-

#  Copyright (C) 2021 Guillaume Jadin
import sys, textwrap, eyed3, argparse
import tools_editor as tools
from tools_editor import Paths
from os import walk, mkdir, rename
from os.path import join, exists, basename
from shutil import move, rmtree
from datetime import datetime

class Colors(tools.Colors):
    """
    Collection of colors to colorize logs
    """
    @staticmethod
    def basic():
        if args.treatment == "full":
            return Colors.YELLOW
        return Colors.CYAN

    @staticmethod
    def full():
        return Colors.GREEN

class Logs(tools.Logs):
    """
    Display Logs in a structured way
    """
    @staticmethod
    def options(sep):
        """
        Display the options for the current run

        :param int sep: the separator size
        """
        if args.analysis == "basic": analysis = args.analysis + " and no-nfo"
        else: analysis = args.analysis
        if args.clean:
            print("    Clear " + f"{Colors.BLUE}" + analysis + f"{Colors.RESET} files", file=sys.stderr)
        else:
            print("    Analyse " + f"{Colors.BLUE}" + analysis + f"{Colors.RESET} files", file=sys.stderr)
            print("    Treat   " + f"{Colors.BLUE}" + args.treatment + f"{Colors.RESET} files", file=sys.stderr)
        print("-" * sep, file=sys.stderr)

    @staticmethod
    def results(nbr_type_files):
        """
        Display the results of the program

        :param list[int] nbr_type_files: a list with the nbr of successes/fulls, warnings/basics and errors
        """
        if args.rename:
            Logs.resume([("edited", nbr_type_files[0], Colors.GREEN),
                         ("unchanged", nbr_type_files[1], Colors.YELLOW),
                         ("incorrect", nbr_type_files[2], Colors.RED)])
        elif args.clean:
            Logs.resume([("cleaned", nbr_type_files[0], Colors.PURPLE),
                         ("unchanged", nbr_type_files[1], Colors.YELLOW)])
        else:
            Logs.resume([("full(s)", nbr_type_files[0], Colors.full()),
                         ("basic(s)", nbr_type_files[1], Colors.basic())])

    @staticmethod
    def result(status_code, filename):
        """
        Display the result for one file

        :param int status_code: full/cleaned metadata (0), basic/unchanged metadata (1)
        :param str filename: a filename
        """
        if args.clean:
            if status_code == 0:
                Logs.status(("CLEANED", filename, Colors.PURPLE))
            elif status_code == 1:
                Logs.status(("UNCHANGED", filename, Colors.YELLOW))
        else:
            if status_code == 0:
                Logs.status_file(filename, ("FULL", Logs.full_msg(), Colors.full()))
            elif status_code == 1:
                Logs.status_file(filename, ("BASIC", Logs.basic_msg(), Colors.basic()))

    @staticmethod
    def basic_msg():
        if args.treatment == "full": return "FILE skipped"
        return "NFO created "

    @staticmethod
    def full_msg():
        return " NFO created "

    @staticmethod
    def not_found_dir(filename, path_music):
        """
        Display a warning if no folder has found.

        :param str filename: a filename
        :param str path_music: the current music path
        """
        Logs.pcolor("Can't find a DIRECTORY for \"{0}\"\n\t{1}".format(filename, path_music), Colors.YELLOW)

    @staticmethod
    def not_found_song(filename, path_music):
        """
        Display a warning if no song has found.

        :param str filename: a filename
        :param str path_music: the current music path
        """
        Logs.pcolor("Can't find a SONG for \"{0}\"\n\t{1}".format(filename, path_music), Colors.YELLOW)

    @staticmethod
    def not_found_img(filename, path_music):
        """
        Display a warning if no image has found.

        :param str filename: a filename
        :param str path_music: the current music path
        """
        Logs.pcolor("Can't find an IMAGE for \"{0}\"\n\t{1}".format(filename, path_music), Colors.YELLOW)

    @staticmethod
    def edit_err(file):
        """
        Display the file in red, an error as occurred in the file structure

        :param str file: a file
        """
        Logs.pcolor(file, Colors.RED)

    @staticmethod
    def edit_unchanged(file):
        """
        Display the file in yellow, it has not been edited

        :param str file: a file
        """
        Logs.pcolor(file, Colors.YELLOW)

    @staticmethod
    def edit_changed(file):
        """
        Display the file in green, it has been edited

        :param str file: a file
        """
        Logs.pcolor(file, Colors.GREEN)

class FileParser(tools.FileParser):
    """
    Functions which help to manage filename
    """
    @staticmethod
    def filter_files(curr_path, files, wanted_ext_files):
        """
        Keep files in the list "files" which have an extension equal at "wanted_ext_files"

        :param str curr_path: the path where the files are
        :param list[str] files: a filenames list
        :param list[str] wanted_ext_files: a list of strings with the format ".EXTENSION" (EX: '.mp3', '.mp4', '.nfo', ...)
        :return: a list with all the filenames with the extension "wanted_ext_file".
                 if there is no elements with the extension "wanted_ext_file", return an empty list [].
        :rtype: list[str]
        """
        correct_ext = []
        for file in files:
            filename, curr_ext = FileParser.sep_extension(file)
            if curr_ext in wanted_ext_files:
                if args.analysis == "no-nfo" or args.analysis == "basic":
                    if not files.__contains__("{0}.nfo".format(filename)):
                        correct_ext.append(file)
                else:
                    correct_ext.append(file)

                if args.analysis == "basic":
                    if files.__contains__(filename + ".nfo"):
                        path_file = join(curr_path, "{0}.nfo".format(filename))
                        if FileParser.__is_basic_nfo(path_file, ["track", "album", "genre", "year"]):
                            correct_ext.append(file)
        return correct_ext

    @staticmethod
    def __is_basic_nfo(path_file, headers):
        """
        Check if a (music video) nfo is basic (not complete) or full (complete)

        :pre: the nfo file must exist
        :param str path_file: the path to the nfo file
        :param list[str] headers: a list of header name which make the difference between a basic and a full nfo
        :return: True if basic, False else
        :rtype: bool
        """

        def parse_headers():
            """
            Parse the headers name to isolate values more easily

            :return: a list of headers parsed
            :rtype: list[list[str, str]]
            """
            parsed = []
            for name in headers:
                parsed.append(["<{0}>".format(name), "</{0}>".format(name)])
            return parsed

        no_metadata_counter = 0
        with open(path_file, "r", encoding="utf-8", errors='replace') as nfo_file:
            for line in nfo_file.readlines():
                line = line.strip(" \n")
                for header in parse_headers():
                    if line[:line.find(">") + 1] == header[0] and line[line.rfind("</"):] == header[1]:
                        value = line.strip(header[0]).rstrip(header[1])
                        if value == "":
                            no_metadata_counter += 1
                        break
        return no_metadata_counter == 4

    @staticmethod
    def create_folder(curr_path, file):
        """
        Create a new folder for music videos which are in the same path as the "Path.MUSIC_VIDEOS" path

        :param str curr_path: the path where the file is
        :param str file: a file (a filename with an extension)
        :return: the path to the new created folder, else, the "curr_path" parameter.
        :rtype: str
        """
        if curr_path == Paths.MUSIC_VIDEOS:
            new_path = join(curr_path, FilenameEditor.delete_forbidden_char(FileParser.sep_extension(file)[0]))
            try:
                mkdir(new_path)
                move(join(curr_path, file), join(new_path, file))
            except FileExistsError:
                pass
            return new_path
        return curr_path

class FilenameEditor(tools.FilenameEditor):
    """
    Functions which help to edit music videos files.
    """
    @staticmethod
    def run(curr_path, file):
        """
        Edit music videos filename: remove useless words or characters

        :param str curr_path: the path where the file is
        :param str file: a file (a filename with an extension)
        :return: a status code: edited (0), not edited (1)
        :rtype: int
        """
        status_code, new_file = 1, file
        if FilenameEditor.has_strange_char(file):
            status_code, new_file = FilenameEditor.replace_strange_char(status_code, file)
            if FilenameEditor.has_strange_char(file):
                Logs.edit_err(file)
                return
        sub_new_file = FileParser.sep_extension(new_file)
        status_code, new_file = FilenameEditor.delete_labels(status_code, sub_new_file[0],
                                                             FilenameEditor.get_music_videos_labels())
        new_file += sub_new_file[1]

        if status_code == 0:
            rename(join(curr_path, file), join(curr_path, new_file))
            Logs.edit_changed(file)
        else:
            Logs.edit_unchanged(file)
        return status_code

    @staticmethod
    def remove_feat_info(status_code, filename):
        """
        Remove the featuring information (used to correspond music videos in musics database)

        :param int status_code: edited (0), not edited (1)
        :param filename: a filename (a file WITHOUT an extension)
        :return: the (un)changed status code and the (un)edited filename
        :rtype: (bool, str)
        """
        new_sc, new_filename = FilenameEditor.delete_labels(status_code, filename, FilenameEditor.get_feat_labels() + FilenameEditor.get_music_videos_labels())
        return (new_sc == 0), new_filename

class FileMatcher(tools.FileMatcher):
    """
    Functions which help to match folders/files between the music videos directory and the musics directory.
    """
    @staticmethod
    def artist_dir(path, wanted_dir):
        """
        Search if an artist directory exists in the current path (the music directory)

        :param str path: the path of the music directory (where to search)
        :param str wanted_dir: the artist name took in the music videos directory
        :return: if found the name of the correct directory else None
        :rtype: str | None
        """
        wanted_dir_parsed = FilenameEditor.delete_forbidden_char(wanted_dir.rstrip("."))
        return FileMatcher.search_dir(path, wanted_dir_parsed)

    @staticmethod
    def song_file(path, wanted_file):
        """
        Search if a song file exists in the current path (the music directory)

        :param str path: the path of the music directory (where to search)
        :param str wanted_file: the song name took in the music videos directory (without extension)
        :return: if found the name of the correct file: (is_m_parsed, filename) else (is_m_parsed, None)
        :rtype: (bool, str) | (bool, None)
        """
        return FileMatcher.search_file(path, "{0}.mp3".format(wanted_file), delete_labels=True)

def Cleaner(curr_path, file):
    """
    Move the music video file in the parent directory and delete the current folder.
    (Apply only on files with the same name as the music video filename)

    :param curr_path: the path where the music video file is
    :param file: the music video file
    :return: a status code: cleaned file (0), not cleaned file (1)
    :rtype int
    """
    filename, ext = FileParser.sep_extension(file)
    curr_dir = basename(curr_path)
    status_code = 1
    if curr_dir != '' and FilenameEditor.delete_forbidden_char(filename) == curr_dir:
        move(join(curr_path, file), join(Paths.MUSIC_VIDEOS, file))
        rmtree(curr_path)
        status_code = 0
    Logs.result(status_code, filename)
    return status_code

def Arguments():
    parser = argparse.ArgumentParser(description="Create nfo for music videos and search metadata in music library",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-w", "--warning", action="store_true",
                        help="Display warnings to the user (logs in YELLOW)")

    parser.add_argument("-r", "--rename", action="store_true",
                        help="""FILENAME EDITOR MODE: Rename music videos to remove useless words in filename.
    (Standalone)""")

    parser.add_argument("-c", "--clean", action="store_true",
                        help="""CLEANER MODE: Clear all folders and keep music videos files only.
    (Standalone)""")

    parser.add_argument("-a", "--analysis", type=str, default="basic", choices=("all", "basic", "no-nfo"),
                        help=textwrap.dedent("""Define on which type of music videos the program will analyse:
    \t 'all'    -> Analyse all music videos files,
    \t 'basic'  -> Analyse only music videos files which have NOT a nfo OR an incomplete nfo,
    \t 'no-nfo' -> Analyse only music videos files which have NOT a nfo."""))

    parser.add_argument("-t", "--treatment", type=str, default="full", choices=("all", "full"),
                        help=textwrap.dedent("""Define on which type of music videos the program will work:
    \t 'all'    -> Treat all music videos files,
    \t 'full'   -> Treat only music videos files which have a FULL label."""))
    parser.add_argument("-mdb", "--musicDB", type=str, default=None, help="Define the music path.")
    parser.add_argument("-mvdb", "--musicvideoDB", type=str, default=None, help="Define the music video path.")
    return parser.parse_args()

class Runner:
    """
    Launcher class which run the program when the constructor is called.
    """
    __show_warning = None

    def __init__(self):
        """
        Launch the Program:
            1. Loop on all the music videos in the music videos directory,
            2. Work only on mp4, mkv files,
            3. Generate nfo for all the files with the artist name and the song title,
            4. Add to each nfo more metadata if a similar file is found in the mp3 library.
        """
        eyed3.log.setLevel("ERROR")  # Avoid the displaying of warnings provided by the eyed3 module.
        if not exists(Paths.MUSIC_VIDEOS):
            raise FileExistsError("The MUSIC_VIDEOS path doesn't exists: \"{0}\"".format(Paths.MUSIC_VIDEOS))
        if (Paths.MUSICS is not None) and (not exists(Paths.MUSICS)):
            if not args.rename and not args.clean:
                raise FileExistsError("The MUSICS path doesn't exists: \"{0}\"".format(Paths.MUSICS))

        self.__show_warning = args.warning
        nbr_type_files = [0, 0, 0]
        for curr_path, dirs, files in sorted(walk(Paths.MUSIC_VIDEOS, topdown=False)):
            if curr_path == Paths.MUSIC_VIDEOS and len(files) > 0:
                print("-- Files in Folder --", file=sys.stderr)

            for file_filtered in sorted(FileParser.filter_files(curr_path, files, [".mp4", ".mkv"])):
                if args.rename:
                    status_code = FilenameEditor.run(curr_path, file_filtered)
                    nbr_type_files[status_code] += 1
                elif args.clean:
                    status_code = Cleaner(curr_path, file_filtered)
                    nbr_type_files[status_code] += 1
                else:
                    type_file = self.__create_nfo(curr_path, file_filtered)
                    nbr_type_files[type_file] += 1

            if curr_path == Paths.MUSIC_VIDEOS and len(dirs) > 0:
                if len(files) > 0: print(file=sys.stderr)
                print("-- Files in Sub-Folders --", file=sys.stderr)
        Logs.results(nbr_type_files)

    def __create_nfo(self, curr_path, file):
        """
        Create a nfo file the directory "curr_dir" with the same filename as "file".

        :param str curr_path: the path where the music video file is
        :param str file: the music video file
        :return: a status code: full metadata (0), basic metadata (1)
        :rtype int
        """
        status_code, metadata = self.__get_music_metadata(curr_path, file)
        filename = FileParser.sep_extension(file)[0]
        if not(args.treatment == "full" and status_code == 1):
            curr_path = FileParser.create_folder(curr_path, file)
            nfo = open(join(curr_path, "{0}.nfo".format(filename)), "w")
            nfo.write("""<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>\n""")
            nfo.write("<musicvideo>\n")
            nfo.write("    <title>{0}</title>\n".format(metadata.get("title")))
            nfo.write("    <artist>{0}</artist>\n".format(metadata.get("artist")))
            nfo.write("    <track>{0}</track>\n".format(metadata.get("track")))
            nfo.write("    <album>{0}</album>\n".format(metadata.get("album")))
            nfo.write("    <genre>{0}</genre>\n".format(metadata.get("genre")))
            nfo.write("    <year>{0}</year>\n".format(metadata.get("year")))
            nfo.write("    <dateadded>{0}</dateadded>\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
            nfo.write("</musicvideo>")
            nfo.close()
        Logs.result(status_code, filename)
        return status_code

    def __get_music_metadata(self, curr_dir, file):
        """
        Search in the music directory if a mp3 file corresponding with "file" already exists
        to extract its metadata and add it for the music video "file".

        :param str curr_dir: the directory where the music video file is
        :param str file: a music video file (often with a mp4 extension)
        :return: a status code: full metadata (0), basic metadata (1)
                 a dictionary with all the metadata founded in the mp3 file
        :rtype: (int, dict[str, str])
        """
        def parse(data):
            return "" if data is None else str(data)

        filename, ext = FileParser.sep_extension(file)
        artist, title = FileParser.sep_artist_title(filename)
        meta = {
            "artist": artist,
            "title": title,
            "year": "",
            "album": "",
            "genre": "",
            "track": "",
        }
        if Paths.MUSICS is None:
            return 1, meta
        curr_path_music = Paths.MUSICS

        # Parsing current MUSIC VIDEO
        is_mv_parsed, parsed_filename = FilenameEditor.remove_feat_info(1, filename)

        if exists(join(curr_path_music, artist[0].upper())):                        # Check if ARTIST exists
            curr_path_music = join(curr_path_music, artist[0].upper())

        artist_dir_searched = FileMatcher.artist_dir(curr_path_music, artist)
        if artist_dir_searched is None:  # ARTIST NOT FOUND
            if self.__show_warning:
                Logs.not_found_dir(filename, curr_path_music)
            return 1, meta
        curr_path_music = join(curr_path_music, artist_dir_searched)

        is_m_parsed, song_file_searched = FileMatcher.song_file(curr_path_music, parsed_filename) # Check if SONG exists
        if song_file_searched is None:   # SONG NOT FOUND
            if self.__show_warning:
                Logs.not_found_song(parsed_filename, curr_path_music)
            return 1, meta
        curr_path_music = join(curr_path_music, song_file_searched)

        mp3file_tags = eyed3.load(curr_path_music).tag
        song_file_searched = FileParser.sep_extension(song_file_searched)[0]

        best_title = title
        # Uncomment if user needs to choose between mv or m (in case of mv hasn't FEAT. and m has)
        # if (not is_mv_parsed) and is_m_parsed:
        #     if Logs.icolor("Use Music Metadata [M=>y / MV=>N] \n\tMV => {mv}\n\tM  => {m}\n"
        #                               .format(mv=filename, m=song_file_searched), Colors.YELLOW).lower() == "y":
        #         best_title = FileParser.sep_artist_title(song_file_searched)[1]

        meta["title"] = best_title
        meta["artist"] = parse(mp3file_tags.artist)
        meta["year"] = parse(mp3file_tags.getBestDate())
        meta["album"] = parse(mp3file_tags.album)
        meta["genre"] = parse(mp3file_tags.genre)
        meta["track"] = mp3file_tags.track_num
        have_img = False
        for nbr, img_album in enumerate(mp3file_tags.images):
            if nbr == 0: nbr = ""; have_img = True
            img_file = open(join(FileParser.create_folder(curr_dir, file),
                                 "{0}-fanart{1}.jpg".format(filename, str(nbr))), "wb")
            img_file.write(img_album.image_data)
            img_file.close()

        if self.__show_warning and not have_img:
            Logs.not_found_img(filename, curr_path_music)
        if meta.get("genre") != "":
            meta["genre"] = meta.get("genre")[meta.get("genre").find(")") + 1:]
        if meta.get("track") is not None:
            meta["track"] = parse(meta.get("track")[0])
        return 0, meta

if __name__ == "__main__":
    # This program will print logs on stderr during the nfo creation file with 2 different headers:
    #     [FULL]  => Corresponding music file     found + extract wanted metadata.
    #     [BASIC] => Corresponding music file NOT found + only extract the artist name and the song title

    args = Arguments()

    if args.musicDB is not None:
        Paths.MUSICS = args.musicDB
    if args.musicvideoDB is not None:
        Paths.MUSIC_VIDEOS = args.musicvideoDB
    Paths.MUSICS.encode('unicode_escape')
    Paths.MUSIC_VIDEOS.encode('unicode_escape')
    sep_size = Logs.paths(need_m=False if (args.clean or args.rename) else True)

    if args.rename:
        answer = input(f"Launch the {Colors.YELLOW}FILENAME EDITOR{Colors.RESET} mode? [y/N] ")
    elif args.clean:
        Logs.options(sep_size)
        answer = input(f"Launch the {Colors.YELLOW}CLEANER{Colors.RESET} mode? [y/N] ")
    else:
        Logs.options(sep_size)
        answer = input(f"Launch the {Colors.YELLOW}NFO GENERATOR{Colors.RESET} mode)? [y/N] ")

    if answer.lower() == "y":
        Runner()
    else:
        Logs.abort()
