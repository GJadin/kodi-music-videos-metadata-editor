#!/usr/bin/python3
# -*- coding: utf-8 -*-

#  Copyright (C) 2021 Guillaume Jadin
from os import walk, system, remove, rename
from os.path import join, exists
import eyed3, argparse
from pydub import AudioSegment, effects
import tools_editor as tools
from tools_editor import Colors, Paths

def Arguments():
    parser = argparse.ArgumentParser(description="Extract & Treat Audio from Music Videos",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-e", "--extract", action="store_true",
                        help="""EXTRACT MODE: Extract all audio from music videos files""")
    parser.add_argument("-n", "--normalize", action="store_true",
                        help="""NORMALIZE MODE: Normalize all audio files""")
    parser.add_argument("-a", "--assemble", action="store_true",
                        help="""ASSEMBLE MODE: Assemble all audio into music videos files""")
    parser.add_argument("-c", "--clean", action="store_true", help="""CLEANER MODE: Clear all wav files""")
    parser.add_argument("-mvdb", "--musicvideoDB", type=str, default=None, help="Define the music video path.")
    return parser.parse_args()

def run():
    """
    Launcher class which run the program when the constructor is called.
    """
    eyed3.log.setLevel("ERROR")  # Avoid the displaying of warnings provided by the eyed3 module.
    if not exists(Paths.MUSIC_VIDEOS):
        raise FileExistsError("The MUSIC_VIDEOS path doesn't exists: \"{0}\"".format(Paths.MUSIC_VIDEOS))

    curr_path, dirs, files = sorted(walk(Paths.MUSIC_VIDEOS, topdown=False))[0]
    for file_filtered in sorted(tools.FileParser.filter_files(curr_path, files, ".mp4")):
        filename = tools.FileParser.sep_extension(file_filtered)[0]
        if not (filename[len(filename)-4:] == "_bak" and args.clean):
            if filename[len(filename)-4:] == "_bak":
                filename = filename[:len(filename) - 4]

            if args.extract:
                rename(join(curr_path, file_filtered), join(curr_path, filename+"_bak.mp4"))
                system("""ffmpeg -i "{0}_bak.mp4" -vn "{0}.wav" """.format(join(curr_path, filename)))
            
            if args.normalize and exists(join(curr_path, "{0}.wav".format(filename))):
                raw_sound_file = AudioSegment.from_file(join(curr_path, filename+".wav"), "wav")
                normalized_sound = effects.normalize(raw_sound_file)
                rename(join(curr_path, filename+".wav"), join(curr_path, filename+"_bak.wav"))
                normalized_sound.export(join(curr_path, filename+".wav"), format="wav")

            if args.assemble and exists(join(curr_path, "{0}_bak.mp4".format(filename))) and exists(join(curr_path, "{0}.wav".format(filename))):
                system("""ffmpeg -i "{0}_bak.mp4" -i "{0}.wav" -vcodec copy -map 0:v:0 -map 1:a:0 -acodec aac "{0}.mp4" """
                          .format(join(curr_path, filename)))

            if args.clean:
                if exists(join(curr_path, "{0}_bak.mp4".format(filename))): remove(join(curr_path, "{0}_bak.mp4".format(filename)))
                if exists(join(curr_path, "{0}_bak.wav".format(filename))): remove(join(curr_path, "{0}_bak.wav".format(filename)))
                if exists(join(curr_path, "{0}.wav".format(filename))): remove(join(curr_path, "{0}.wav".format(filename)))

if __name__ == "__main__":
    args = Arguments()
    if args.musicvideoDB is not None:
        Paths.MUSIC_VIDEOS = args.musicvideoDB
    Paths.MUSICS.encode('unicode_escape')
    Paths.MUSIC_VIDEOS.encode('unicode_escape')
    sep_size = 0

    modes = ""

    if args.extract:
        modes += f"{Colors.GREEN}EXTRACTOR{Colors.RESET} "
    if args.normalize:
        modes += f"{Colors.BLUE}NORMALIZE{Colors.RESET} "
    if args.assemble:
        modes += f"{Colors.YELLOW}ASSEMBLY{Colors.RESET} "
    if args.clean:
        modes += f"{Colors.PURPLE}CLEANER{Colors.RESET} "
    if len(modes) > 0:
        sep_size = tools.Logs.paths(need_m=False)
        answer = input(f"Launch the {modes}mode? [y/N] ")
    else:
        tools.Logs.pcolor("error: No command specified\nTry './audio_extractor --help' for more information.", Colors.RED)
        exit(1)

    if answer.lower() == "y":
        run()
    else:
        tools.Logs.abort()
